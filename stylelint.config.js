module.exports = {
  plugins: [
    'stylelint-a11y',
    'stylelint-no-unsupported-browser-features',
    'stylelint-scss',
  ],
  processors: [
    [
      '@mapbox/stylelint-processor-arbitrary-tags',
      { fileFilterRegex: [/\.vue$/] },
    ]
  ],
  extends: [
    'stylelint-a11y/recommended',
    'stylelint-config-recommended',
    'stylelint-config-recommended-scss',
    'stylelint-config-recess-order',
  ],
  rules: {
    'no-empty-source': null,
  },
  ignoreFiles: [
    'coverage/**/*',
  ],
};
