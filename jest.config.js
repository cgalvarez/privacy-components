// If you face the error `Requires Babel "^7.0.0-0", but was loaded with "6.26.3"`,
// then follow steps detailed in:
//   https://www.stephencharlesweiss.com/2018-12-16/error-handling-jest-with-babel/

module.exports = {
  moduleFileExtensions: ['js', 'json', 'vue'],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '^.+\\.js$': 'babel-jest',
  },
  snapshotSerializers: ['jest-serializer-vue'],
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  collectCoverage: true,
  collectCoverageFrom: [
    "**/*.{js,vue}",
    "!*.config.js",
    "!**/*.stories.js",
    "!**/coverage/**",
    "!**/node_modules/**"
  ],
  coverageReporters: [
    'html',
    'lcov',
    'text-summary',
  ],
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: -10,
    },
  },
};
