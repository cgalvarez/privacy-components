module.exports = {
  branches: [
    '+([0-9])?(.{+([0-9]),x}).x',
    'master',
    'next',
    'next-major',
    { name: 'beta', prerelease: true },
    { name: 'alpha', prerelease: true },
  ],
  preset: 'angular',
  plugins: [
    [
      '@semantic-release/commit-analyzer',
      {
        parserOpts: {
          noteKeywords: [
            'BREAKING CHANGE',
            'BREAKING CHANGES',
            'BREAKING',
          ],
        },
      },
    ],
    [
      '@semantic-release/release-notes-generator',
      {
        parserOpts: {
          noteKeywords: [
            'BREAKING CHANGE',
            'BREAKING CHANGES',
            'BREAKING',
          ],
        },
        writerOpts: {
          commitsSort: [
            'subject',
            'scope',
          ],
        },
      },
    ],
    [
      '@semantic-release/changelog',
      {
        changelogFile: 'CHANGELOG.md',
      },
    ],
    '@semantic-release/npm',
    '@semantic-release/git',
    [
      '@semantic-release/gitlab',
      {
        /*'gitlabUrl": "https://custom.gitlab.com",
        assets: [
          {
            path: 'dist/asset.min.css',
            label: 'CSS distribution',
          },
          {
            path: 'dist/asset.min.js',
            label: 'JS distribution',
          },
        ],*/
      },
    ],
    //'@semantic-release/error',
  ],
};
