import { configure, addDecorator, addParameters } from '@storybook/vue';
import { withA11y } from '@storybook/addon-a11y';
import { themes, create } from '@storybook/theming';
import { addReadme } from 'storybook-readme/vue';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';


const basicTheme = create({
  base: 'light',
  brandTitle: 'Privacy Components',
  brandUrl: 'https://gitlab.com/cgalvarez/privacy-components',
  brandImage: null,
});

// Setup storybook-readme.
addParameters({
  options: {
    showPanel: true,
    panelPosition: 'right',
    theme: basicTheme,
  },
  readme: {
    codeTheme: 'github',
  },
});

// Setup storybook-dark-mode.
// See https://github.com/hipstersmoothie/storybook-dark-mode#story-integration
// TODO: Adjust dark mode: change text color.
addParameters({
  darkMode: {
    dark: { ...themes.dark, appBg: 'black' },
    light: { ...themes.normal, appBg: 'white' },
  },
});

// Setup viewport.
// See https://github.com/storybookjs/storybook/tree/master/addons/viewport#add-new-device
addParameters({
  viewport: {
    viewports: {
      ...INITIAL_VIEWPORTS,
    },
  },
});

addDecorator(addReadme);
addDecorator(withA11y);

// automatically import all files ending in *.stories.js
const req = require.context('../src', true, /\.stories\.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
