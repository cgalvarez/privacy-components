# Privacy components

<table>
  <tr>
    <td>Project</td>
    <td>
      <a href="https://gitlab.com/cgalvarez/privacy-components/blob/dev/LICENSE" title="Project license">
        <img src="https://badgen.net/badge/license/MIT/blue" alt="License badge" />
      </a>
      <a href="https://opensource.org/">
        <img src="https://img.shields.io/badge/Open%20Source-♥-ff69b4.svg?style=flat"
          alt="FLOSS badge" />
      </a>
      <a href="./code-of-conduct.md" title="Contributor covenant: A code of conduct for open source projects">
        <img src="https://img.shields.io/badge/Contributor%20Covenant%20CoC-v1.4%20adopted-ff69b4.svg" alt="Contributor Covenant" />
      </a>
      <a href="http://makeapullrequest.com" title="Make a pull request">
        <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat&logo=gitlab" alt="PRs welcome badge" />
      </a>
      <br />
      <a href="http://semver.org/spec/v2.0.0.html" title="Semantic versioning 2.0.0 specification">
        <img src="https://badgen.net/badge/SemVer/2.0.0/blue" alt="SemVer badge" />
      </a>
      <a href="https://semantic-release.gitbook.io/semantic-release/" title="Project following the semantic release workflow">
        <img src="https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg" alt="Semantic release badge" />
      </a>
      <a href="http://commitizen.github.io/cz-cli/">
        <img src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat"
          alt="Commitizen friendly" />
      </a>
      <br />
      <a href="https://npmjs.com/package/privacy-components" title="Package version @ NPM">
        <img src="https://img.shields.io/npm/v/privacy-components.svg?logo=npm" alt="Latest npm version badge">
      </a>
      <a href="https://www.code-inspector.com/public/project/193/Privacy%20components/dashboard" title="CodeInspector quality report">
        <img src="https://www.code-inspector.com/project/193/status/svg" alt="CodeInspector grade badge" />
      </a>
      <a href="https://img.shields.io/snyk/vulnerabilities/npm/privacy-components.svg" title="Snyk vulnerabilites for NPM package">
        <img src="https://img.shields.io/snyk/vulnerabilities/npm/privacy-components.svg?logo=snyk" alt="Snyk badge" />
      </a>
    </td>
  <tr>
    <td><code>master</code></td>
    <td>
      <a href="https://npmjs.com/package/privacy-components" title="Package privacy-components@latest at NPM">
        <img src="https://img.shields.io/npm/v/privacy-components/latest.svg?logo=npm" alt="npm@latest badge" />
      </a>
      <a href="https://app.codeship.com/projects/351268"
          title="Codeship CI status for gitlab:cgalvarez/privacy-components@master">
        <img src="https://img.shields.io/codeship/8d2e86d0-7c66-0137-28b5-723c935256fa/master.svg?label=ci%2Fcodeship%20@%20master&logo=codeship"
            alt="Codeship build badge for master branch" />
      </a>
      <a href="https://codecov.io/gl/cgalvarez/privacy-components/branch/master"
          title="Codecov code coverage status for gitlab:cgalvarez/privacy-components@master">
        <img src="https://badgen.net/codecov/c/gitlab/cgalvarez/privacy-components/master?icon=codecov&label=codecov%20@%20master"
            alt="Codecov coverage badge for master branch" />
      </a>
      <a href="https://coveralls.io/gitlab/cgalvarez/privacy-components"
          title="Coveralls code coverage status for gitlab:cgalvarez/privacy-components@master">
        <img src="https://coveralls.io/repos/gitlab/cgalvarez/privacy-components/badge.svg?branch=master"
            alt="Coveralls coverage badge for master branch" />
      </a>
      <a href="https://codebeat.co/projects/gitlab-com-cgalvarez-privacy-components-master">
        <img src="https://codebeat.co/badges/a8512eb3-58b3-49fe-8bc9-eeb2397c5ed7" alt="codebeat grade badge for master branch" />
      </a>
      <a href="http://retire.insecurity.today/api/image?uri=https://gitlab.com/cgalvarez/privacy-components/raw/master/package.json"
          title="Retire.js vulnerabilities">
        <img src="http://retire.insecurity.today/api/image?uri=https://gitlab.com/cgalvarez/privacy-components/raw/master/package.json"
            alt="retire badge for master branch" />
      </a>
    </td>
  </tr>
  <tr>
    <td><code>next</code></td>
    <td>
      <a href="https://npmjs.com/package/privacy-components" title="Package privacy-components@next at NPM">
        <img src="https://img.shields.io/npm/v/privacy-components/next.svg?logo=npm" alt="npm@next badge" />
      </a>
      <a href="https://app.codeship.com/projects/351268"
          title="Codeship CI status for gitlab:cgalvarez/privacy-components@next">
        <img src="https://img.shields.io/codeship/8d2e86d0-7c66-0137-28b5-723c935256fa/next.svg?label=ci%2Fcodeship%20@%20next&logo=codeship"
            alt="Codeship build badge for next branch" />
      </a>
      <a href="https://codecov.io/gl/cgalvarez/privacy-components/branch/next"
          title="Codecov code coverage status for gitlab:cgalvarez/privacy-components@next">
        <img src="https://badgen.net/codecov/c/gitlab/cgalvarez/privacy-components/next?icon=codecov&label=codecov%20@%20next"
            alt="Codecov coverage badge for next branch" />
      </a>
      <a href="https://coveralls.io/gitlab/cgalvarez/privacy-components"
          title="Coveralls code coverage status for gitlab:cgalvarez/privacy-components@next">
        <img src="https://coveralls.io/repos/gitlab/cgalvarez/privacy-components/badge.svg?branch=next"
            alt="Coveralls coverage badge for next branch" />
      </a>
      <a href="https://codebeat.co/projects/gitlab-com-cgalvarez-privacy-components-next">
        <img src="https://codebeat.co/badges/3aed5e49-f92d-4709-8313-3654a1c8c22a" alt="codebeat grade badge for next branch" />
      </a>
      <a href="http://retire.insecurity.today/api/image?uri=https://gitlab.com/cgalvarez/privacy-components/raw/next/package.json"
          title="Retire.js vulnerabilities">
        <img src="http://retire.insecurity.today/api/image?uri=https://gitlab.com/cgalvarez/privacy-components/raw/next/package.json"
            alt="retire badge for next branch" />
      </a>
    </td>
  </tr>
</table>

Privacy components provides privacy-focused components to use on a variety of frontend UIs/frameworks.

## License

[MIT](LICENSE) &copy; Carlos García 2019
