/* eslint-env jest */
import { mount } from '@vue/test-utils';
import ConsentAlert from '../ConsentAlert.vue';

describe('ConsentAlert', () => {
  it('is a Vue instance', () => {
    const wrapper = mount(ConsentAlert);
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('renders the correct markup', () => {
    const wrapper = mount(ConsentAlert);
    expect(wrapper.find('.btn.opt-in').text()).toBe('Accept');
    expect(wrapper.find('.btn.opt-out').text()).toBe('Decline');
    expect(wrapper.find('.link').text()).toBe('Read our privacy policy.');
    expect(wrapper.element).toMatchSnapshot();
  });

  it('emits "optIn" event once when accepting cookie usage by clicking "Accept" button', () => {
    const wrapper = mount(ConsentAlert);
    const button = wrapper.find('.btn.opt-in');
    const event = 'optIn';
    expect(wrapper.emitted(event)).toBeFalsy();
    button.trigger('click');
    expect(wrapper.emitted(event)).toBeTruthy();
    expect(wrapper.emitted(event).length).toBe(1);
    expect(wrapper.emitted(event)[1]).toEqual(undefined);
  });

  it('emits "optOut" event once when declining cookie usage by clicking "Decline" button', () => {
    const wrapper = mount(ConsentAlert);
    const button = wrapper.find('.btn.opt-out');
    const event = 'optOut';
    expect(wrapper.emitted(event)).toBeFalsy();
    button.trigger('click');
    expect(wrapper.emitted(event)).toBeTruthy();
    expect(wrapper.emitted(event).length).toBe(1);
    expect(wrapper.emitted(event)[1]).toEqual(undefined);
  });

  it('emits "moreInfo" event once when user requests more information by clicking the link', () => {
    const wrapper = mount(ConsentAlert);
    const button = wrapper.find('.link');
    const event = 'moreInfo';
    expect(wrapper.emitted(event)).toBeFalsy();
    button.trigger('click');
    expect(wrapper.emitted(event)).toBeTruthy();
    expect(wrapper.emitted(event).length).toBe(1);
    expect(wrapper.emitted(event)[1]).toEqual(undefined);
  });

  it('changes text labels through prop `labels`', () => {
    const labels = {
      btn: {
        optIn: 'Yeah, dude!',
        optOut: 'No way, sir!',
      },
      link: 'I want to know more.',
    };
    const wrapper = mount(ConsentAlert, {
      propsData: { labels },
    });
    expect(wrapper.find('.btn.opt-in').text()).toBe(labels.btn.optIn);
    expect(wrapper.find('.btn.opt-out').text()).toBe(labels.btn.optOut);
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    expect(wrapper.find('.link').text()).toBe(labels.link);
  });

  it('changes content through slots', () => {
    const slots = {
      default: 'Watch out! Cookies in use!',
      'btn-decline': '<b>No way, sir!</b>',
      'btn-accept': '<i>Yeah, dude!</i>',
    };
    const wrapper = mount(ConsentAlert, { slots });
    const html = wrapper.html();
    expect(html).toContain(`<div class="btn opt-in">${slots['btn-accept']}</div>`);
    expect(html).toContain(`<div class="btn opt-out">${slots['btn-decline']}</div>`);
    expect(wrapper.find('.message').html()).toContain(slots.default);
  });

});
