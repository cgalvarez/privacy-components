import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';

import ConsentAlert from './ConsentAlert';
import ConsentAlertReadme from './README.md';


storiesOf('Components|Consent alert', module)
  .addParameters({
    readme: {
      sidebar: ConsentAlertReadme,
    },
  })

  .add('with defaults',
    () => ({
      components: { ConsentAlert },
      template: `<consent-alert @optIn="onOptIn" @optOut="onOptOut" @moreInfo="onMoreInfo">
                  We use cookies to provide our services.
                </consent-alert>`,
      methods: {
        onMoreInfo: action('moreInfo'),
        onOptIn: action('onOptIn'),
        onOptOut: action('onOptOut'),
      },
    }),
    {
      notes: {
        markdown: 'This simple example renders the consent alert with the default options.',
      },
    },
  )

  .add('with custom content using slots',
    () => ({
      components: { ConsentAlert },
      template: `
        <consent-alert @optIn="onOptIn" @optOut="onOptOut" @moreInfo="onMoreInfo">
          We use cookies to provide our services.
          <template v-slot:btn-decline>
            <i class="mdi mdi-thumb-down"></i> &nbsp; No, thanks
          </template>
          <template v-slot:btn-accept>
            <i class="mdi mdi-thumb-up"></i> &nbsp; Yeah!
          </template>
        </consent-alert>
      `,
      created() {
        this.loadCdnStylesheet('https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css');
        this.loadCdnStylesheet('https://cdn.jsdelivr.net/npm/roboto-fontface@0.10.0/css/roboto/roboto-fontface.css');
      },
      methods: {
        onMoreInfo: action('moreInfo'),
        onOptIn: action('onOptIn'),
        onOptOut: action('onOptOut'),
        loadCdnStylesheet(src) {
          const link = document.createElement('link');
          link.type = 'text/css';
          link.rel = 'stylesheet';
          link.href = src;
          document.head.appendChild(link);
        },
        loadCdnScript(src) {
          const script = document.createElement('script');
          //script.setAttribute('src', src);
          script.src = src;
          document.head.appendChild(script);
        },
      },
    }),
    {
      notes: {
        markdown: 'This example shows how to leverage the component slots to render a custom content inside the buttons.',
      },
    },
  );
