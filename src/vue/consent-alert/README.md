# Vue Consent Alert

This component aims to show the typical alert informing the user about the cookies usage by the site.

## Usage

```javascript
<template>
  <consent-alert prop="value" :prop="variable" @event="callback">
    This is the text shown in the alert.
  </consent-alert>
</template>

<script>
  import 'ConsentAlert' from 'privacy-components/src/vue/consent-alert/ConsentAlert';

  export default {
    name: 'my-custom-comp',

  };
</script>
```

## Properties

Property | Type | Default | Description
--- | --- | --- | ---
theme | Object | - | Applies the provided styles to the alert.
labels | Object | `btn.optIn = 'Accept'`<br>`btn.optOut = 'Decline'`<br>`link = 'Read privacy policy'` | The labels to use.

## Slots

Slot | Description
--- | ---
*default* | The contents for the message shown above buttons.
btn-accept | The contents for the *accept* button.
btn-decline | The contents for the *decline* button.

## Events

Event | Extra arguments | Emitten when...
--- | --- | ---
moreInfo | - | ...user clicks the *more information* link.
optIn | - | ...user accepts the use of cookies.
optOut | - | ...user denies the use of cookies.

## Styles

Customized styles can be applied to any of the HTML components through next classes:

CSS class | Applied to...
--- | ---
`.consent-alert` | ...the alert container.
`.message` | ...the main message (the one in the main slot).
`.link` | ...the *more information* link.
`.btn` | ...both buttons, *accept* and *decline*.
`.opt-out` | ...the *decline* button.
`.opt-in` | ...the *accept* button.
